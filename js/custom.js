(function ($) {

  //var backButton = $();
  //var nextButton = $('.next-button');
  var postContainer = $('#blog-carousel .et_pb_ajax_pagination_container');

  postContainer.addClass('slider');
  postContainer.addClass('blog-carousel');

  $(document).ready(function () {
    console.log('hello');
    $(".blog-carousel .et_pb_post").each(function () {
      $(this).find(".entry-title, .post-meta, .published, .post-content ").wrapAll('<div class="carousel-posts-text"></div>');
    });

    //Do the same for ajax
    $(document).bind('ready ajaxComplete', function () {
      $(".blog-carousel .et_pb_post").each(function () {
        $(this).find(".entry-title, .post-meta, .published, .post-content ").wrapAll('<div class="carousel-posts-text"></div>');
      });
    });
    $(".slick-active .carousel-posts-text").addClass("cs-hidden");
    $(".slick-current.slick-active.slick-center .carousel-posts-text").removeClass("cs-hidden");

    $('.slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
      $(".slick-active .carousel-posts-text").addClass("cs-hidden");
      $(".slick-current.slick-active.slick-center .carousel-posts-text").addClass("cs-hidden");
    });
    $('.slider').on('afterChange', function (event, slick, currentSlide) {
      $(".slick-active .carousel-posts-text").addClass("cs-hidden");
      $(".slick-current.slick-active.slick-center .carousel-posts-text").removeClass("cs-hidden");
    });

  });
  $('.blog-carousel').slick({
    infinite: true,
    //arrows: true,
    appendArrows: ".blog-carousel",
    slidesToShow: 3,
    slidesToScroll: 1,
    centerMode: true,
    centerPadding: '0',
    useCSS: true,
    useTransform: true,
    swipe: true,
    swipeToSlide: true,

    prevArrow: '<span class="back-button">D</span>',
    nextArrow: '<span class="next-button">E</span>',

    responsive: [{
      breakpoint: 740, settings: {
        slidesToShow: 1
      }
    }]

  });


})(jQuery);

(function ($) {
  var menuHeight = $("#global-header-section").height();
  var $scrollMenuSpacing = $("#cs-scrollmenu");
  $scrollMenuSpacing.css('margin-top', menuHeight);
  var topPosition = 0;


  $(window).scroll(function () {

    var scrollMovement = $(window).scrollTop();

    if (topPosition < 200) {
    }
    else {
      if (scrollMovement > topPosition) {
        $('#global-header-section').removeClass('show-header');
        $('#global-header-section').addClass('hide-header');
        $scrollMenuSpacing.css('margin-top', 0);
      } else {
        $('#global-header-section').removeClass('hide-header');
        $('#global-header-section').addClass('show-header');
        $scrollMenuSpacing.css('margin-top', menuHeight);
      }
    }
    topPosition = scrollMovement;
  });

})(jQuery);

(function ($) {
  $(document).ready(function () {
    var docHeight = $(document).height();
    var winHeight = $(window).height();
    var $scrollBar = $('#scrollBar');
    var $progressLabel = $('.et-progress-label p span');
    $scrollBar.css('width', 0);
    $progressLabel.html(' ');
    $(window).scroll(function () {
      var winScrollTop = $(window).scrollTop();
      var scrollPercentage = Math.abs(winScrollTop / (docHeight - winHeight) * 100);
      $scrollBar.css('width', (scrollPercentage + '%'));
      $progressLabel.html(Math.round(scrollPercentage) + '%');
    });
  });
})(jQuery);

jQuery(document).on('ready ajaxComplete', function () {
  //Replace read more link text
  jQuery(".et_pb_post a.more-link").html(function () {
    return jQuery(this).html().replace('read more', 'Leia mais');
  });
});

(function ($) {
  $(document).ready(function () {
    console.log('hello');
    $(".cs-slider .et_pb_slides .et_pb_slide").each(function () {
      $(this).find(".et_pb_slide_image, .et_pb_slide_content").wrapAll('<div class="cs_img_description"></div>');
    });
  });
})(jQuery);

(function ($) {

  $(document).ready(function () {
    
    $("#cs_carousel-3d article").each(function(){
      var $dataId = $(this).attr('id');
      $(this).find(".entry-featured-image-url").each(function(){
        $(this).attr('data-id', $dataId)
      });
    });
    

    $("#cs_carousel-3d").filter(function () {
      $(this).find('article').eq(0).addClass('active');
      $(this).find('article').index(0);
      $(this).find("article .entry-featured-image-url")
      .detach()
      .prependTo(".et_pb_ajax_pagination_container")
      .wrapAll("<div class='cs_img-carousel'></div>");

      $(this).find("article").wrapAll('<div class="carousel-text"></div>');
     
    });

    $('.cs_img-carousel .entry-featured-image-url:eq(0)').addClass('current-slide');
      

    const carousel = document.querySelector('.cs_img-carousel');
    const cells = carousel.querySelectorAll('.entry-featured-image-url');
    const cellCount = cells.length;
    const cellWidth = carousel.offsetWidth;
    const lastSlideIndex = cells.length - 1;
    let selectedIndex = 0;
    
 
    let radius, theta;
    
    const currentSlidesClasses = currentSlide => {
      cells.forEach(cell => cell.classList.remove('current-slide'))
      cells[currentSlide].classList.add('current-slide');
    }
    
    function textDisplay(){   
      $('.entry-featured-image-url.current-slide').filter(function (){
        let imgDataID = $(this).attr('data-id');
        $('.carousel-text article').each(function (){ 
          let textID = $(this).attr('id');
          if (textID === imgDataID) {
            $(this).fadeIn(200).addClass("active");
          } else {
            $(this).fadeOut(200).removeClass("active");
          }
      })   
        // console.log($(this).attr('data-id'));
      })
    };
   
    function rotateCarousel() {
      
      theta = 360 / cellCount;
      // console.log(theta + ' theta');
      let cellSize =  cellWidth;
      radius = Math.round( ( cellSize / 2) / Math.tan( Math.PI / cellCount ) );
      for ( let i=0; i < cells.length; i++ ) {
        let cell = cells[i];
        // console.log(cells[i].getAttribute('data-id'));
        if ( i < cellCount ) {
          // visible cell
          cell.style.opacity = 1;
          
          let cellAngle = theta * i;
          // console.log(cellAngle);
          cell.style.transform =  'rotateY(' + -cellAngle + 'deg) translateZ(' + -radius  + 'px)';
        } else {
          // hidden cell
          cell.style.opacity = 0;
          cell.style.transform = 'none';
         
        }
      }
      let angle = theta * selectedIndex * -1;
      carousel.style.transform = 'translateZ(' + radius  + 'px) ' + 
      'rotateY(' + -angle + 'deg)';

      textDisplay();
  
    };

    $("<button class='previous-button'>D</button>").insertBefore('.carousel-text');
    $('<button class="next-button">E</button>').insertAfter('.carousel-text');

    $prevButton = $('.previous-button');
    $prevButton.on( 'click', function() {
    // if ( selectedIndex === 0) {
    //   selectedIndex = cells.length -1
    // } else {
    //   selectedIndex--;
    // }

    const currentSlide = selectedIndex === 0
      ? selectedIndex = lastSlideIndex
      : --selectedIndex
    
    currentSlidesClasses(currentSlide)
    rotateCarousel();
    });

    $nextButton = $('.next-button');
    $nextButton.on( 'click', function() {
    // if(selectedIndex === cells.length -1) {
    //   selectedIndex = 0
    // } else {
    //   selectedIndex++
    // }
    const currentSlide = selectedIndex === lastSlideIndex
      ? selectedIndex = 0
      : ++selectedIndex

    currentSlidesClasses(currentSlide)
    rotateCarousel();
    });
   
  });
})(jQuery);